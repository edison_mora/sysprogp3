#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>

struct Persona {
    char nombre[50];
    char apellido[50];
    int edad;
};

struct Persona personas[3];

int verificarEdad(int edad);
int verificarNombre(char * nombre);
void promedioEdad(struct Persona personas[]);
void personaMayor(struct Persona personas[]);
void personaMenor(struct Persona personas[]);

int main() {
    int contador = 0;
    int flagNombre = 0;
    int flagApellido = 0;
    int flagEdad = 0;
    int promedio = 0;
    while(contador < 3){
        printf("Persona #%d \n", contador);
        while(flagNombre==0){
            printf("Ingrese su nombre: ");
            scanf("%s", personas[contador].nombre);
            if(verificarNombre(personas[contador].nombre) == 1){
                flagNombre = 1;
            }
        }
        while(flagApellido==0){
            printf("Ingrese su apellido: ");
            scanf("%s", personas[contador].apellido);
            if(verificarNombre(personas[contador].apellido) == 1){
                flagApellido = 1;
            }
        }
        while(flagEdad==0){
            printf("Ingrese su edad: ");
            scanf("%d", &personas[contador].edad);
            if(verificarEdad(personas[contador].edad)==1){
                flagEdad = 1;
            }
        }
        contador ++;
        flagApellido = flagEdad = flagNombre = 0;
    }
    for (int i = 0; i < 3; i++) {
        printf("---------------\n");
        printf("Persona #%d\n", i);
        printf("Nombre: %s\nApellido: %s\nEdad: %d\n", personas[i].nombre, personas[i].apellido, personas[i].edad);
    }
    promedioEdad(personas);
    personaMayor(personas);
    personaMenor(personas);
    //return 0;
}

int verificarEdad(int edad){
    //printf("Verificando edad: %d\n", edad);
    if(((edad>=0)&&(edad<=130))){
        //printf("Edad correcta.\n");
        return 1;
    }else{
        printf("Ingrese una edad entre 0 y 130 años.\n");
        return 0;
    }
}

int verificarNombre(char * nombre){
    int rc;
    regex_t * myregex;
    rc = regcomp(myregex, "^([A-Z])([a-z])+", REG_EXTENDED | REG_NOSUB);
    //printf("RC from regcomp() = %d\n", rc);
    //printf("Verificando el nombre: %s\n", nombre);
    if(!regexec(myregex, nombre, 0, 0, 0)){
        //printf("Correcto");
        return 1;
    }else{
        printf("La primera letra debe ser mayúscula\n");
        return 0;
    }
}

void promedioEdad(struct Persona personas[]){
    int edades = 0;
    int length = 3;
    for (int i = 0; i < 3; i++) {
        edades = edades + personas[i].edad;
    }
    printf("\nLa suma de edades es: %d", edades);
    int promedio = (double)edades/length;
    printf("\nEl promedio es: %d", promedio);
}

void personaMayor(struct Persona personas[]){
    struct Persona temp = personas[0];
    for (int i = 0; i < 3; i++) {
        if(personas[i].edad > temp.edad){
            temp = personas[i];
        }
    }
    printf("\nLa persona más vieja es: %s %s, edad: %d\n", temp.nombre, temp.apellido, temp.edad);
}

void personaMenor(struct Persona personas[]){
    struct Persona temp = personas[0];
    for (int i = 0; i < 3; i++) {
        if(personas[i].edad < temp.edad){
            temp = personas[i];
        }
    }
    printf("\nLa persona más joven es: %s %s, edad: %d\n", temp.nombre, temp.apellido, temp.edad);
}